# Google Sign-In Demo

Simple application to demonstrate sign in - sign out with Google API.

![](./sign-in-with-google.png)


- - - -


### Development server

Run `npm run start` for a dev server. 
Navigate to `http://localhost:4200/`. The app will automatically reload if you change 
any of the source files.


- - - -


### Links

* **Lite server**: [repo](https://github.com/johnpapa/lite-server)
* **Google Sign-In** 
  * [Integration](https://developers.google.com/identity/sign-in/web/sign-in)
  * [Auth with a backend](https://developers.google.com/identity/sign-in/web/backend-auth)
  * [Developer console](https://console.developers.google.com)
